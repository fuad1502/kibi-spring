FROM eclipse-temurin:19-jdk-alpine AS build
WORKDIR /workspace/app/
COPY gradle/ gradle/
COPY gradlew .
RUN ./gradlew init
COPY build.gradle .
COPY settings.gradle .
COPY src/ src/
RUN ./gradlew bootJar

FROM eclipse-temurin:19-jdk-alpine AS run
WORKDIR /bin
COPY --from=build /workspace/app/build/libs/*jar .
ENTRYPOINT ["sh", "-c", "java -jar *.jar"]
